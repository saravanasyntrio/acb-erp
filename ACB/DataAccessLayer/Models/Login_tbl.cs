﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models
{
   public class Login_tbl
    {
        [Key]  //DataAnnotations    
        [Column(Order = 0)]   // order from db column align
        public int id { get; set; }

        [Column(Order = 1), Required, MaxLength(25), MinLength(2)]
        public string uname { get; set; }  // nvarchar(Max)--chance to slow

        [Column(TypeName = "varchar(50)", Order = 2), MaxLength(15), MinLength(8)]      // define the datatype
        public string passwrd { get; set; }

        [Column("u_role", TypeName = "varchar(15)", Order = 3)]
        public string role { get; set; }

        [Column("u_status", TypeName = "varchar(10)", Order = 4)]
        public string status { get; set; }

        [Column(TypeName = "varchar(20)", Order = 5)]
        public string Token_id { get; set; }
    }
}
