﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ACB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace ACB.Controllers.WEB_pages
{
    public class Country_viewController : Controller
    {
        
        string baseurl = "http://localhost:7499/";   // api base url
        private readonly INotyfService _notyf;
        public Country_viewController(INotyfService notyf)
        {
            _notyf = notyf;
        }
        public ActionResult Index()   // view with model binding by saravana
        {
            try
            {

                String url = baseurl + "Country/Cntry_view";
                using (var wc = new System.Net.WebClient())
                    url = wc.DownloadString(url);
                var data = JsonConvert.DeserializeObject<List<CountryViewModel>>(url);
                _notyf.Success("Showing the Country List");
                return View(data);
            }
            catch {
                _notyf.Error("Failed");
            }

            return View();
        }
        public ActionResult Multi_view()   // inner join with model binding
        {
            try
            {
                String url = baseurl + "Country/multi_join";
                using (var wc = new System.Net.WebClient())
                    url = wc.DownloadString(url);
                var data = JsonConvert.DeserializeObject<List<MultiViewModel>>(url);
                return View(data);
            }
            catch {
                _notyf.Error("Failed");
            }
            return View();
        }
        public ActionResult Create([Bind("lastname")] insert_country country)   // insert
        {
            try
            {

                if (ModelState.IsValid)
                {
                    String url = baseurl + "Country/Create?lastname=" + country.lastname;

                    using (var wc = new System.Net.WebClient())
                        url = wc.DownloadString(url);
                    var data = JsonConvert.DeserializeObject(url);
                    string data1 = Convert.ToString(data);
                    if (data1 == "1")
                    {
                        _notyf.Success("Successfully Inserted");
                    }
                    else
                    {
                        _notyf.Error("Failed");
                    }
                    return Redirect("Index");
                }
            }
            catch {
                _notyf.Error("Failed");
            }
            return View();
        }

        public ActionResult Delete(int? id)   // delete
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                else
                {

                    String url = baseurl + "Country/Delete_cntry?id=" + id;

                    using (var wc = new System.Net.WebClient())
                    {
                        url = wc.DownloadString(url);
                        var data = JsonConvert.DeserializeObject(url);
                        string data1 = Convert.ToString(data);
                        if (data1 == "1")
                        {
                            _notyf.Warning("Successfully Deleted");
                        }
                        else
                        {
                            _notyf.Error("Failed");
                        }
                        return Redirect("/Country_view/Index");
                    }

                }
            }
            catch {
                _notyf.Error("Failed");
            }
            return View();
        }

        public async Task<IActionResult> Edit(int? id)  // Edit
        {
            try
            {

                if (id == null)
                {
                    return NotFound();
                }

                String url = baseurl + "Country/Details_cntry?id=" + id;
                using (var wc = new System.Net.WebClient())
                    url = wc.DownloadString(url);
                var data = JsonConvert.DeserializeObject<CountryViewModel>(url);

                _notyf.Warning("Check the value and update");
                if (url == null)
                {
                    return NotFound();
                }
                return View(data);
            }
            catch {
                _notyf.Error("Failed");
            }
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,lastname")] CountryViewModel country)   // update
        {
            try
            {
                if (id != country.id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    String url = baseurl + "Country/Update_cntry?id=" + country.id + "&lastname=" + country.lastname;

                    using (var wc = new System.Net.WebClient())
                        url = wc.DownloadString(url);
                    var data = JsonConvert.DeserializeObject(url);
                    string data1 = Convert.ToString(data);
                    if (data1 == "1")
                    {
                        _notyf.Success("Successfully Updated");
                        return Redirect("/Country_view/Index");
                    }
                    else
                    {
                        _notyf.Error("Updation Failed");
                        //  return View();
                    }

                }
            }
            catch {
                _notyf.Error("Failed");
            }
            return View();
        }

        public ActionResult Find_country(string Name)   
        {
            if (Name != null)
            {

               
                return Redirect("/Country_view/search_country?name="+Name);
            }
            else {
                Index();
            }

            return View();
        }
        public ActionResult search_country(string Name)
        {

                String url = baseurl + "Country/Details_cntry_string?name=" + Name;
                using (var wc = new System.Net.WebClient())
                    url = wc.DownloadString(url);
                var data = JsonConvert.DeserializeObject<List<CountryViewModel>>(url);

                  return View(data);
               
        }
    }
}
