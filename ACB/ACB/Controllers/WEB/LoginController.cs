﻿using BusinessLayer.BusinessLogic;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACB.Controllers.WEB
{
    public class LoginController : Controller
    {
        private readonly Login_in _bhj_lg;
        public LoginController(Login_in bhj_lg)
        {
            _bhj_lg = bhj_lg;
        }
        public IActionResult Index()
        {
            return View();
        }
        public string Create(BusinessLayer.Abstractclass.Login_tbl lg)   // insert Login
        {
            var jj = _bhj_lg.create(lg);
            return jj;
        }
         public List<Login_tbl> Details_login_details(string uname,string passwrd, string u_status)   // return login table details 
        {
        return _bhj_lg.Details_login(uname, passwrd, u_status);
}
        public JsonResult Details_login(string uname, string passwrd, string u_status)  // if success return username or failes
        {
            var result= _bhj_lg.Details_login(uname, passwrd, u_status);
            string username;
            if (result.Count > 0) {
                username = uname;
            }
            else {
                username = "Failed";
            }
              return Json(username);
        }
    }
}
