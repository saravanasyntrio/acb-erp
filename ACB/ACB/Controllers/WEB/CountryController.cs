﻿using BusinessLayer.BusinessLogic;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ACB.Controllers.WEB
{
    public class CountryController : Controller
    {
        private readonly Country_in _bhj_cntry;
        public CountryController(Country_in bhj_cntry){
            _bhj_cntry = bhj_cntry;
        }
        public IActionResult Index()
        {
            return View();
        }
       

        public string Create(BusinessLayer.Abstractclass.country country)   // insert country
        {
            var jj = _bhj_cntry.create(country);
            string result = Convert.ToString(jj);
            
            return jj;
        }
       
        public List<country> Cntry_view()   // view all the country details
        {
           
            return _bhj_cntry.Cntry_view();

        }
        
        public JsonResult Details_cntry(int id)  // view particular country details
        {
            var jj = _bhj_cntry.Details_cntryAsync(id);
             return Json(jj);
        }
        public List<country> Details_cntry_string(string name)  // view particular country details based on string value
        {
           return _bhj_cntry.Details_cntryAsync_string(name);
            
        }
        public string Update_cntry(int id, BusinessLayer.Abstractclass.country cntry)  // update country details
        {
            var jj = _bhj_cntry.Update_cntry(id, cntry);
           
            return jj;
        }

        public string Delete_cntry(int id) {   // delete country details
            var jj = _bhj_cntry.Delete_cntry(id);

            return jj;

        }
        public List<country> Cntry_view_desc()   // order desc of country details
        {

            return _bhj_cntry.Cntry_view_desc();

        }
        public List<country> Cntry_view_asc()   // order asc of country details
        {

            return _bhj_cntry.Cntry_view_asc();

        }
        public List<country> Cntry_view_asc_two()   // fetch asc top 2 rows of country details
        {

            return _bhj_cntry.Cntry_view_asc_two();

        }
        public List<country> Cntry_view_desc_two()   // fetch last 2 rows of country details
        {

            return _bhj_cntry.Cntry_view_desc_two();

        }
        public List<country> Cntry_view_two()   // generally fetch first tow rows(without asc) of country details
        {

            return _bhj_cntry.Cntry_view_two();

        }
        public List<country> multi_tbl() // joins with conditions
        {    
            return _bhj_cntry.multi_tbl();
        }
        public List<BusinessLayer.Abstractclass.multi_join> multi_join() // inner join--good one
        { 
            return _bhj_cntry.multi_join();
            
        }
        public List<BusinessLayer.Abstractclass.multi_join> multi_join_inner() // inner join 
        {
            return _bhj_cntry.multi_join();

        }
        public List<BusinessLayer.Abstractclass.multi_join> multi_join_left() // left outer join 
        {
            return _bhj_cntry.multi_join_left();

        }
        public List<BusinessLayer.Abstractclass.multi_join> multi_join_right() // right outer join 
        {
            return _bhj_cntry.multi_join_right();

        }

    }
}
