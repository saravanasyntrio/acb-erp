#pragma checksum "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b4b56c5a4de95682a50c75243b616e8687db814d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Country_view_Multi_view), @"mvc.1.0.view", @"/Views/Country_view/Multi_view.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\saravan\acb\ACB\ACB\Views\_ViewImports.cshtml"
using ACB;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\saravan\acb\ACB\ACB\Views\_ViewImports.cshtml"
using ACB.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b4b56c5a4de95682a50c75243b616e8687db814d", @"/Views/Country_view/Multi_view.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8d22b1e92742ddbbf5c4673b9868d7c82a3b95ef", @"/Views/_ViewImports.cshtml")]
    public class Views_Country_view_Multi_view : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<ACB.Models.MultiViewModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml"
  
    ViewData["Title"] = "Inner Join";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("<table class=\"table\">\r\n    <thead>\r\n        <tr>\r\n            <th>\r\n                ");
#nullable restore
#line 10 "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml"
           Write(Html.DisplayNameFor(model => model.lastname));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 13 "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml"
           Write(Html.DisplayNameFor(model => model.uname));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 16 "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml"
           Write(Html.DisplayNameFor(model => model.status));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
#nullable restore
#line 22 "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
#nullable restore
#line 26 "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml"
           Write(Html.DisplayFor(modelItem => item.lastname));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 29 "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml"
           Write(Html.DisplayFor(modelItem => item.uname));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 32 "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml"
           Write(Html.DisplayFor(modelItem => item.status));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n\r\n        </tr>\r\n");
#nullable restore
#line 36 "C:\Users\saravan\acb\ACB\ACB\Views\Country_view\Multi_view.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\r\n</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<ACB.Models.MultiViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
