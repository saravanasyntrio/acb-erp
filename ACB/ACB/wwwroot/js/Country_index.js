﻿$(document).ready(function () {
    //$("#btnsrch").click(function () {
    //    $.ajax(
    //        {
    //            type: "POST", //HTTP POST Method
    //            url: "Find_country", // Controller/View
    //            data: { //Passing data
    //                Name: $("#CountryName").val(), //Reading text box values using Jquery

    //            },


    //        });

    //});
    var $rows = $('.table tr');
    $('#CountryName').keyup(function () {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });
});