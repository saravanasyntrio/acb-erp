
//using DataAccessLayer;
//using DataAccessDBContext;
using AspNetCoreHero.ToastNotification;
using BusinessLayer.BusinessLogic;
using DataAccessLayer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // our code start
            // services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));
            services.AddDbContext<ApplicationDbContext>(
                options =>
                {
                    options.UseSqlServer(Configuration.GetConnectionString("DbConnectSql"));
                }
                );

            services.AddDbContext<BusinessContext.ApplicationDbContext1>(
               options =>
               {
                   options.UseSqlServer(Configuration.GetConnectionString("DbConnectSql"));
               }
               );

            services.AddTransient<Country_in, Countrylogic>();
            services.AddTransient<Login_in, Loginlogic>();


            //    services.AddEntityFrameworkSqlServer()
            //.AddDbContext<DataAccessDBContext.ApplicationDbContext>(options =>
            //  options.UseSqlServer(Configuration.GetConnectionString("DbConnectSql)")));


            // services.AddMvc();

            services.AddNotyf(config =>
            {
                config.DurationInSeconds = 10;
                config.IsDismissable = true;
                config.Position = NotyfPosition.TopRight;
            });
            // our code end

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
