﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACB.Models
{
    public class Response_json
    {
        
            public string status { get; set; }
            public string message { get; set; }

            public CountryViewModel data { get; set; }

    }
}
