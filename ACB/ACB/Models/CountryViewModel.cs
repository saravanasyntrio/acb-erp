﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace ACB.Models
{
    public class CountryViewModel
    {
        public int id { get; set; }
        public string lastname { get; set; }
     //   public string CountryName { get; set; }

    }
    public class MultiViewModel
    {
        public string lastname { get; set; }
        public string uname { get; set; }
        public string status { get; set; }

    }
    public class insert_country
    {
        [ Required, MaxLength(25), MinLength(2)]
        public string lastname { get; set; }
    }
    public class search_country
    {
        public string CountryName { get; set; }
    }
}
