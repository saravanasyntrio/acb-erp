﻿using DataAccessLayer.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLayer.BusinessLogic
{
   public interface Country_in
    {
       public string create(BusinessLayer.Abstractclass.country cntry);
        public List<country> Cntry_view();
        public country Details_cntryAsync(int id);
        public List<country> Details_cntryAsync_string(string name);
        public string Update_cntry(int id, BusinessLayer.Abstractclass.country cntry);
        public string Delete_cntry(int id);
        public List<country> Cntry_view_desc();
        public List<country> Cntry_view_asc();
        public List<country> Cntry_view_asc_two();
        public List<country> Cntry_view_desc_two();
        public List<country> Cntry_view_two();
        public List<country> multi_tbl();
        public List<BusinessLayer.Abstractclass.multi_join> multi_join();
        public List<BusinessLayer.Abstractclass.multi_join> multi_join_left();
        public List<BusinessLayer.Abstractclass.multi_join> multi_join_right();
        public List<BusinessLayer.Abstractclass.multi_join> multi_join_inner();
    }
}
