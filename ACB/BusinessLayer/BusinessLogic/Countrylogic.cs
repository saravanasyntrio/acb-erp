﻿//using BusinessLayer.Abstractclass;
using DataAccessLayer.Models;
using DataAccessLayer; 
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BusinessLogic
{
    public class Countrylogic : Country_in
    {
        private readonly ApplicationDbContext _context;
        public Countrylogic(ApplicationDbContext context)
        {
            _context = context;

        }
        
        public string create(BusinessLayer.Abstractclass.country cntry) {
            string count1 = "";
            try
            {
                DataAccessLayer.Models.country obj_cntry = new DataAccessLayer.Models.country();
                obj_cntry.lastname = cntry.lastname;
                _context.Country.Add(obj_cntry);
                var count = _context.SaveChanges();
                 count1 = Convert.ToString(count);
                
            }
            catch {
                count1 = "0";
            }
            return count1;
        }
        public List<country> Cntry_view()
        {

            return _context.Country.ToList();

        }

        public country Details_cntryAsync(int id)
        {

            var country1 = _context.Country.Find(id);
            //    string country2 =Convert.ToString(country1.id +"," +country1.lastname);
            return country1;


        }
        public List<country> Details_cntryAsync_string(string name)
        {
            
            var data = _context.Country.Where(x => x.lastname == name).ToList();
          
            return data;


        }
        public string Update_cntry(int id, BusinessLayer.Abstractclass.country cntry) {
            string count1 = "";
            try
            {

                DataAccessLayer.Models.country obj_cntry = new DataAccessLayer.Models.country();
                obj_cntry.id = id;
                obj_cntry.lastname = cntry.lastname;

                _context.Country.Update(obj_cntry);
                var count = _context.SaveChanges();
                 count1 = Convert.ToString(count);
            }
            catch {
                count1 = "0";
            }
            return count1;
        }

        public string Delete_cntry(int id) {
            string count1 = "";
            try
            {
                var cntry = _context.Country.Find(id);
                _context.Country.Remove(cntry);
                var count = _context.SaveChanges();
                 count1 = Convert.ToString(count);
            }
            catch {
                count1 = "0";
            }
            return count1;
        }
        public List<country> Cntry_view_desc()
        {
            return _context.Country.OrderByDescending(b => b.id).ToList();

        }
        public List<country> Cntry_view_asc()
        {
            return _context.Country.OrderBy(b => b.id).ToList();

        }
        public List<country> Cntry_view_asc_two()
        {
            return _context.Country.OrderBy(b => b.id).Take(2).ToList();
        }
        public List<country> Cntry_view_desc_two()
        {
            return _context.Country.OrderByDescending(b => b.id).Take(2).ToList();
        }
        public List<country> Cntry_view_two()
        {

            return _context.Country.Take(2).ToList();

        }
        public List<country> multi_tbl()
        {
            
            var multi = (from c in _context.Country join l in _context.Login_tbl on c.id equals l.id where c.lastname == "saravana" select c).ToList();
            return multi;

        }
        public List<BusinessLayer.Abstractclass.multi_join> multi_join()
        {
              var details = new List<BusinessLayer.Abstractclass.multi_join>();
            var multi = _context.Country.Join(
                _context.Login_tbl,
                c => c.id,
                l => l.id,
                (c, l) => new { c.lastname, l.uname, l.status }).ToList();
            foreach (var item in multi)
            {
                BusinessLayer.Abstractclass.multi_join mul = new BusinessLayer.Abstractclass.multi_join();
                mul.lastname = item.lastname;
                mul.uname = item.uname;
                mul.status = item.status;
                  details.Add(mul);
            }

            return details;


        }
        public List<BusinessLayer.Abstractclass.multi_join> multi_join_left()
        {
            var details = new List<BusinessLayer.Abstractclass.multi_join>();
            var multi = (from c in _context.Country
                         join l in _context.Login_tbl on c.id equals l.id into cnt
                         from Login_tbl in cnt.DefaultIfEmpty()
                         select new
                         {
                             lastname = c.lastname,
                             uname = Login_tbl.uname,
                             u_status = Login_tbl.status

                         });
            foreach (var item in multi)
            {
                BusinessLayer.Abstractclass.multi_join mul = new BusinessLayer.Abstractclass.multi_join();
                mul.lastname = item.lastname;
                mul.uname = item.uname;
                mul.status = item.u_status ;
                details.Add(mul);
            }

            return details;


        }

        public List<BusinessLayer.Abstractclass.multi_join> multi_join_right()
        {
            var details = new List<BusinessLayer.Abstractclass.multi_join>();
            
            var multi = (from l in _context.Login_tbl
                         join c in  _context.Country on l.id equals c.id into lgn
                         from country in lgn.DefaultIfEmpty()
                         select new
                         {
                             lastname = country.lastname,
                             uname = l.uname,
                             u_status = l.status

                         });
            foreach (var item in multi)
            {
                BusinessLayer.Abstractclass.multi_join mul = new BusinessLayer.Abstractclass.multi_join();
                mul.lastname = item.lastname;
                mul.uname = item.uname;
                mul.status = item.u_status;
                details.Add(mul);
            }

            return details;


        }
        public List<BusinessLayer.Abstractclass.multi_join> multi_join_inner()
        {
            var details = new List<BusinessLayer.Abstractclass.multi_join>();

            var multi = (from c in _context.Country
                         join l in _context.Login_tbl on c.id equals l.id 
                         select new
                         {
                             lastname = c.lastname,
                             uname = l.uname,
                             u_status = l.status

                         });
            foreach (var item in multi)
            {
                BusinessLayer.Abstractclass.multi_join mul = new BusinessLayer.Abstractclass.multi_join();
                mul.lastname = item.lastname;
                mul.uname = item.uname;
                mul.status = item.u_status;
                details.Add(mul);
            }

            return details;


        }


    }
}
