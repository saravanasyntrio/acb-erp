﻿//using BusinessLayer.Abstractclass;
using DataAccessLayer;
using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Security.Cryptography;
using System.Text;

namespace BusinessLayer.BusinessLogic
{
   public class Loginlogic:Login_in
    {
        private readonly ApplicationDbContext _context;
        public Loginlogic(ApplicationDbContext context)
        {
            _context = context;
        }

        public string create(BusinessLayer.Abstractclass.Login_tbl lg)
        {
            DataAccessLayer.Models.Login_tbl obj_lg = new DataAccessLayer.Models.Login_tbl();
            obj_lg.uname = lg.uname;
            obj_lg.passwrd = Encrypt(lg.passwrd);
            obj_lg.role = lg.role;
            obj_lg.status = lg.status;
            obj_lg.Token_id = lg.Token_id;
            _context.Login_tbl.Add(obj_lg);
            _context.SaveChanges();
            return "Success";
        }
        public List<Login_tbl> Details_login(string uname,string passwrd,string u_status)
        {
            passwrd = Encrypt(passwrd);
               return _context.Login_tbl.Where(b => b.uname == uname && b.passwrd == passwrd && b.status == u_status).ToList();   // where condition for string value
          //  var con= _context.Login_tbl.Where(b => b.uname == uname && b.passwrd == passwrd && b.status == u_status).ToList();   // where condition for string value
            //if (con.Count > 0) {
            //    con = "1";
            //}
            //else {
            //    con = 2;
            //}
            //return con;
        }

        //  For Password Encryption
        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (System.Security.Cryptography.Aes encryptor = System.Security.Cryptography.Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
    }
}
