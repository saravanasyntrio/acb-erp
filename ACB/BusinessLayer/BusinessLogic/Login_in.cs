﻿using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.BusinessLogic
{
    public interface Login_in
    {
        public string create(BusinessLayer.Abstractclass.Login_tbl lgin);
        public List<Login_tbl> Details_login(string uname, string passwrd, string u_status);
    }
}
