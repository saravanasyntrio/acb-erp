﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BusinessLayer.Abstractclass
{
    public class country
    {
        [Key]  //DataAnnotations    
               // [Column(Order = 0)]   // order from db column align
        public int id { get; set; }
        [Required]       // for notnull
                         //  [Column(Order = 1), Required, MaxLength(25), MinLength(2)]
        public string lastname { get; set; }  // nvarchar(Max)--chance to slow
    }
}
